﻿using esgeeNepaliDateTool;
using NotificationsExtensions;
using NotificationsExtensions.Tiles;
using System;
using System.Collections.Generic;
using System.Globalization;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Notifications;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;


public class Utilities
{

    public static BackgroundTaskRegistration RegisterBackgroundTask(string taskEntryPoint,
                                                                string taskName,
                                                                IBackgroundTrigger trigger,
                                                                IBackgroundCondition condition)
    {
        // Check for existing registrations of this background task.
        foreach (var cur in BackgroundTaskRegistration.AllTasks)
        {

            if (cur.Value.Name == taskName)
            {
                // The task is already registered.
                cur.Value.Unregister(true); //unregister
                break;
                // return (BackgroundTaskRegistration)(cur.Value);
            }
        }

        // Register the background task.

        var builder = new BackgroundTaskBuilder();

        builder.Name = taskName;
        builder.TaskEntryPoint = taskEntryPoint;
        builder.SetTrigger(trigger);

        if (condition != null)
        {
            builder.AddCondition(condition);
        }

        BackgroundTaskRegistration task = builder.Register();
        
        return task;
    }
 
}


