﻿using esgeeNepaliDateTool;
using NotificationsExtensions;
using NotificationsExtensions.Tiles;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Storage;
using Windows.UI.Notifications;

namespace MyBgTask
{
    public sealed class TileUpdateBackgroundTask : IBackgroundTask
    {
        /// <summary>
        /// get content xml to display on tile
        /// </summary>
        /// <returns></returns>
        public static XmlDocument getXml()
        {
            DateTime dt = DateTime.Now;
            NepaliDate nDate = NepaliDate.AD2BS(dt);

            #region Tile Content (content)

            // Construct the tile content
            TileContent content = new TileContent()
            {
                Visual = new TileVisual()
                {
                    TileMedium = new TileBinding()
                    {
                        Content = new TileBindingContentAdaptive()
                        {
                            Children =
                            {
                                new AdaptiveText()
                                {
                                    Text = NepaliDate.getNepaliBaar(dt.DayOfWeek),
                                    HintStyle = AdaptiveTextStyle.Base,
                                    HintAlign = AdaptiveTextAlign.Center
                                },
                                new AdaptiveGroup()
                                {
                                    Children=
                                    {
                                        new AdaptiveSubgroup()
                                        {
                                            Children=
                                            {
                                                 new AdaptiveText(){
                                                        Text = nDate.ToString("d"),
                                                        HintStyle = AdaptiveTextStyle.Subheader,
                                                        HintAlign = AdaptiveTextAlign.Center
                                                    }
                                            }
                                        },
                                        new AdaptiveSubgroup()
                                        {
                                            Children=
                                            {
                                                 new AdaptiveText(){
                                                        Text = nDate.ToString("mmm"),
                                                        HintStyle = AdaptiveTextStyle.Base

                                                    },
                                                 new AdaptiveText()
                                                        {
                                                            Text = nDate.ToString("yyyy"),
                                                            HintStyle = AdaptiveTextStyle.Base
                                                        }
                                            }
                                        }
                                    }
                                },

                                new AdaptiveText()
                                {
                                    Text = dt.ToString("MMM-dd yyyy"),
                                    HintStyle = AdaptiveTextStyle.BaseSubtle,
                                    HintAlign = AdaptiveTextAlign.Center
                                }
                            }
                        }
                    },

                    TileWide = new TileBinding()
                    {
                        Content = new TileBindingContentAdaptive()
                        {
                            Children ={
                                new AdaptiveText()
                                {
                                    Text = nDate.ToString("yyyy mmm d"),
                                    HintStyle = AdaptiveTextStyle.Title
                                },
                                new AdaptiveText()
                                {
                                    Text = nDate.ToString("DDD"),
                                    HintStyle = AdaptiveTextStyle.Title
                                },
                                new AdaptiveText()
                                {
                                    Text = dt.ToString("MMMM-dd, yyyy"),
                                    HintStyle = AdaptiveTextStyle.SubtitleSubtle
                                }
                             }
                        }
                    },
                    TileSmall=new TileBinding()
                    {
                        Content=new TileBindingContentAdaptive()
                        {
                            Children =
                            {
                                new AdaptiveText()
                                {
                                    Text = nDate.ToString("mm/dd"),
                                    HintStyle = AdaptiveTextStyle.Body
                                },
                                
                                new AdaptiveText()
                                {
                                    Text = NepaliDate.getNepaliBaar(dt.DayOfWeek),
                                    HintStyle = AdaptiveTextStyle.Caption
                                }
                            }
                        }
                    }
                }
            };
            #endregion

            return content.GetXml();
        }

        public void Run(IBackgroundTaskInstance taskInstance)
        {
           // Debug.WriteLine("====SG====Background " + taskInstance.Task.Name + " Starting...");

            // Get a deferral, to prevent the task from closing prematurely
            // while asynchronous code is still running.
            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();

            // Update the live tile.
            UpdateTile(true);

        //    await ApplicationData.Current.LocalFolder.CreateFileAsync("thisIsFromBG.txt"
        //, CreationCollisionOption.ReplaceExisting);
            // Inform the system that the task is finished.
            deferral.Complete();
        }


        public static void UpdateTile(bool CustomTile)
        {
           // Debug.WriteLine("====SG====Background task: updating the tile.");

            if (CustomTile)
            {
                var notification = new TileNotification(getXml());
                TileUpdateManager.CreateTileUpdaterForApplication().Update(notification);
                return;
            }
            

            DateTime dt = DateTime.Now;
            NepaliDate nDate = NepaliDate.AD2BS(dt);
            //var tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150Block);
            //var tileArrtibs = tileXml.GetElementsByTagName("text");
            //tileArrtibs[0].AppendChild(tileXml.CreateTextNode(NepaliDate.getNepaliNumber(nDate.Day)));
            //tileArrtibs[1].AppendChild(tileXml.CreateTextNode(NepaliDate.nepMahina2[nDate.Month-1]+", "+NepaliDate.getNepaliNumber(nDate.Year) + "\n" + NepaliDate.getNepaliBaar(dt.DayOfWeek)));

            var tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150Text01);
            var tileArrtibs = tileXml.GetElementsByTagName("text");
            tileArrtibs[0].AppendChild(tileXml.CreateTextNode(nDate.ToString("mmm d")));
            tileArrtibs[1].AppendChild(tileXml.CreateTextNode(nDate.ToString("DDD, yyyy")));
            tileArrtibs[2].AppendChild(tileXml.CreateTextNode(nDate.ToString("MMM d, yyyy")));
            tileArrtibs[3].AppendChild(tileXml.CreateTextNode(dt.ToString("MMM-dd, yyyy")));

            var tileNotification = new TileNotification(tileXml);
            TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);
        }

    }
}
