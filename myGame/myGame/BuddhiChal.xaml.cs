﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace meroGame
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BuddhiChal : Page
    {
        int scale=60;
        int playMode=1,pMode=1,redOnHand=5,blueOnHand=5;
        Windows.UI.Xaml.Shapes.Path current,prev;
        Point p1;
        PointAnimation currAnim;
        bool ball_tapped = false;
        int winner = 0;
        int currPlayer = 1;
        int[,] board = new int[,]
        {
            { 0,8,8,8,1},
            { 1,8,8,8,0},
            { 0,8,8,8,1},
            { 1,8,8,8,0},
            { 0,8,8,8,1},
        };
        int row, col; // current board index
        public BuddhiChal()
        {
            this.InitializeComponent();
            
           // InitializeUI();
           
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            playMode = (int)e.Parameter;
            pMode = playMode;
            double ht = Window.Current.Bounds.Height;
            double wd = Window.Current.Bounds.Width;
            if (ht > wd)
            {
                mainPanel.Orientation = Orientation.Vertical;
                sp1.Orientation = Orientation.Horizontal;
            }
            else
            {
                mainPanel.Orientation = Orientation.Horizontal;
                sp1.Orientation = Orientation.Vertical;
            }
            scale = (int)(Math.Min(ht, wd) / 5);
            gridMain.Width = gridMain.Height = scale * 5;
            InitializeUI();
            changeTurn(true);

            blueBall1.Stroke = blueBall2.Stroke = blueBall3.Stroke = blueBall4.Stroke = blueBall5.Stroke = new SolidColorBrush(Colors.Black);
            redBall1.Stroke = redBall2.Stroke = redBall3.Stroke = redBall4.Stroke = redBall5.Stroke = new SolidColorBrush(Colors.Black);

        }
        private void InitializeUI()
        {
            line1.X1=line1.Y1=line1.X2 = scale * 0.5; line1.Y2 = scale * 4.5;
            line2.X1=line2.X2 = scale * 1.5; line2.Y1 = scale * 0.5;line2.Y2 = scale * 4.5;
            line3.X1 =line3.X2= scale * 2.5; line3.Y1 = scale * 0.5;line3.Y2 = scale * 4.5;
            line4.X1 =line4.X2= scale * 3.5; line4.Y1 = scale * 0.5;line4.Y2 = scale * 4.5;
            line5.X1 = line5.X2 = line5.Y2= scale * 4.5; line5.Y1 = scale * 0.5;

            line6.Y1 = line6.X1 = line6.Y2 = scale * 0.5; line6.X2 = scale * 4.5;
            line7.Y1 = line7.Y2 = scale * 1.5; line7.X1 = scale * 0.5; line7.X2 = scale * 4.5;
            line8.Y1 = line8.Y2 = scale * 2.5; line8.X1 = scale * 0.5; line8.X2 = scale * 4.5;
            line9.Y1 = line9.Y2 = scale * 3.5; line9.X1 = scale * 0.5; line9.X2 = scale * 4.5;
            line10.Y1 = line10.Y2 = line10.X2 = scale * 4.5; line10.X1 = scale * 0.5;

            lineX1.X1 = lineX1.Y1 = scale * 0.5; lineX1.X2 = lineX1.Y2 = scale * 4.5;
            lineX2.X1 = lineX2.Y2 = scale * 0.5; lineX2.X2 = lineX2.Y1 = scale * 4.5;

            lineD1.X1 = lineD1.Y2 = scale * 0.5; lineD1.X2 = lineD1.Y1 = scale * 2.5;
            lineD3.X1 = lineD3.Y2 = scale * 4.5; lineD3.X2 = lineD3.Y1 = scale * 2.5;
            lineD2.X1 = lineD2.Y2 = scale * 2.5; lineD2.X2 = scale * 4.5; lineD2.Y1 = scale * 0.5;
            lineD4.Y1 = lineD4.X2 = scale * 2.5; lineD4.Y2 = scale * 4.5; lineD4.X1 = scale * 0.5;

            if(playMode==1)
            {
                bb1.Center = new Point(line6.X1, line6.Y1);
                bb2.Center = new Point(line8.X1, line8.Y1);
                bb3.Center = new Point(line10.X1, line10.Y1);
                bb4.Center = new Point(line7.X2, line7.Y2);
                bb5.Center = new Point(line9.X2, line9.Y2);

                rb1.Center = new Point(line6.X2, line6.Y2);
                rb2.Center = new Point(line8.X2, line8.Y2);
                rb3.Center = new Point(line10.X2, line10.Y2);
                rb4.Center = new Point(line7.X1, line7.Y1);
                rb5.Center = new Point(line9.X1, line9.Y1);
            }else
            {
                for (int i = 0; i < 5; i++)
                    for (int j = 0; j < 5; j++)
                        board[i, j] = 8;
                bb1.Center = new Point(bb1.Center.X*scale/63,bb1.Center.Y*scale/63);
                bb2.Center = new Point(bb2.Center.X*scale/63,bb2.Center.Y*scale/63);
                bb3.Center = new Point(bb3.Center.X*scale/63,bb3.Center.Y*scale/63);
                bb4.Center = new Point(bb4.Center.X*scale/63,bb4.Center.Y*scale/63);
                bb5.Center = new Point(bb5.Center.X*scale/63,bb5.Center.Y*scale/63);

                rb1.Center = new Point(rb1.Center.X * scale / 63, rb1.Center.Y * scale / 63);
                rb2.Center = new Point(rb2.Center.X * scale / 63, rb2.Center.Y * scale / 63);
                rb3.Center = new Point(rb3.Center.X * scale / 63, rb3.Center.Y * scale / 63);
                rb4.Center = new Point(rb4.Center.X * scale / 63, rb4.Center.Y * scale / 63);
                rb5.Center = new Point(rb5.Center.X * scale / 63, rb5.Center.Y * scale / 63);

            }
            bb1.RadiusX = bb1.RadiusY = scale / 4;
            bb2.RadiusX = bb2.RadiusY = scale / 4;
            bb3.RadiusX = bb3.RadiusY = scale / 4;
            bb4.RadiusX = bb4.RadiusY = scale / 4;
            bb5.RadiusX = bb5.RadiusY = scale / 4;

            rb1.RadiusX = rb1.RadiusY = scale / 4;
            rb2.RadiusX = rb2.RadiusY = scale / 4;
            rb3.RadiusX = rb3.RadiusY = scale / 4;
            rb4.RadiusX = rb4.RadiusY = scale / 4;
            rb5.RadiusX = rb5.RadiusY = scale / 4;

            Storyboard.SetTargetName(pAnimR1, "redBall1");
            Storyboard.SetTargetName(pAnimR2, "redBall2");
            Storyboard.SetTargetName(pAnimR3, "redBall3");
            Storyboard.SetTargetName(pAnimR4, "redBall4");
            Storyboard.SetTargetName(pAnimR5, "redBall5");

            Storyboard.SetTargetName(pAnimB1, "blueBall1");
            Storyboard.SetTargetName(pAnimB2, "blueBall2");
            Storyboard.SetTargetName(pAnimB3, "blueBall3");
            Storyboard.SetTargetName(pAnimB4, "blueBall4");
            Storyboard.SetTargetName(pAnimB5, "blueBall5");
        }
        private void changeTurn(bool isBlue)
        {
            prev = null;
            int st = isBlue ? scale/20 : 0;
            blueBall1.StrokeThickness =blueBall2.StrokeThickness =blueBall3.StrokeThickness =blueBall4.StrokeThickness =blueBall5.StrokeThickness = st;
            st = isBlue ? 0 : scale/20;
            redBall1.StrokeThickness = redBall2.StrokeThickness = redBall3.StrokeThickness = redBall4.StrokeThickness = redBall5.StrokeThickness = st;

            turnOf1.Visibility = isBlue ? Visibility.Visible : Visibility.Collapsed;
            turnOf2.Visibility = isBlue ? Visibility.Collapsed : Visibility.Visible;
        }
        //private bool isValidMove(int from,int to)
        //{
        //    bool result=false;
        //    if(from==1 && to==4) //down right
        //    {
        //        if (board[row + 1, col + 1] == 8)
        //            result = true;
        //    }
        //    else if(from==2 && to==3) // down left
        //    {
        //        if (board[row + 1, col - 1] == 8)
        //            result = true;

        //    }
        //    else if(from==3 && to==2) //top right
        //    {
        //        if (board[row - 1, col + 1] == 8)
        //            result = true;
        //    }
        //    else if(from==4 && to==1)// top left
        //    {
        //        if (board[row - 1, col - 1] == 8)
        //            result = true;
        //    }
        //    return result;
        //}
        private bool isValidMove(direction d)
        {
            bool result = false;
            switch (d)
            {
                case direction.Left:
                    if (board[row, col - 1] == 8)
                        result = true;
                    break;
                case direction.Up:
                    if (board[row - 1, col] == 8)
                        result = true;
                    break;
                case direction.Right:
                    if (board[row, col + 1] == 8)
                        result = true;
                    break;
                case direction.Down:
                    if (board[row + 1, col] == 8)
                        result = true;
                    break;
                case direction.UpLeft:
                    if (board[row - 1, col - 1] == 8)
                        result = true;
                    break;
                case direction.UpRight:
                    if (board[row - 1, col + 1] == 8)
                        result = true;
                    break;
                case direction.DownLeft:
                    if (board[row + 1, col - 1] == 8)
                        result = true;
                    break;
                case direction.DownRight:
                    if (board[row + 1, col + 1] == 8)
                        result = true;
                    break;
                default:
                    break;
            }
            return result;
        }
        Popup popUp;
        private void Grid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if(playMode==2)
                if(redOnHand>0 || blueOnHand>0)
                {
                   Point p = e.GetPosition(gridMain);
                    int c = (int)(p.X / scale);
                    int r = (int)(p.Y / scale);
                    p = new Point((scale / 2) + (c * scale), (scale / 2) + (r * scale));
                    if (board[r, c] == 8)
                        board[r, c] = currPlayer - 1;
                    else
                        return;

                    if (currPlayer==1)
                    {
                        switch (blueOnHand)
                        {
                            case 5:
                                currAnim = pAnimB1;
                                break;
                            case 4:
                                currAnim = pAnimB2;
                                break;
                            case 3:
                                currAnim = pAnimB3;
                                break;
                            case 2:
                                currAnim = pAnimB4;
                                break;
                            case 1:
                                currAnim = pAnimB5;
                                break;

                            default:
                                break;
                        }
                        blueOnHand--;
                    }
                    else
                    {
                        switch (redOnHand)
                        {
                            case 5:
                                currAnim = pAnimR1;
                                break;
                            case 4:
                                currAnim = pAnimR2;
                                break;
                            case 3:
                                currAnim = pAnimR3;
                                break;
                            case 2:
                                currAnim = pAnimR4;
                                break;
                            case 1:
                                currAnim = pAnimR5;
                                break;

                            default:
                                break;
                        }
                        redOnHand--;
                    }
                    currAnim.To = p;
                    
                    //current.StrokeThickness = 0;
                    sbBall.Begin();
                    bool isBlue = currPlayer == 1;
                    currPlayer =  isBlue? 2 : 1;
                    changeTurn(!isBlue);
                    if (redOnHand == 0 && blueOnHand == 0)
                        playMode = 1;
                    if (redOnHand == 0 || blueOnHand == 0)
                        checkResult();
                    return;
                }
            //if(sbBall.Duration=new Duration(new TimeSpan()))

            if (!ball_tapped && p1.X>0)
            {
                
                Point p = e.GetPosition(gridMain);
               // Grid grd = sender as Grid;
                double dx = p.X - p1.X;
                double dy = p.Y - p1.Y;
                direction d;
                if (Math.Abs(Math.Abs(dx) - Math.Abs(dy)) < (scale / 5) ) //move diagonally
                {
                    if ((row + col) % 2 == 0) //able to move diagonally
                    {
                        if (dx > 0 && dy > 0) //down right
                        {
                            d = direction.DownRight;
                        }
                        else if (dx < 0 && dy > 0) //down left
                        {
                            d = direction.DownLeft;
                        }
                        else if (dx > 0 && dy < 0) //up right
                        {
                            d = direction.UpRight;

                        }
                        else //(dx < 0 && dy < 0) //up left
                        {
                            d = direction.UpLeft;
                        }

                        if (isValidMove(d))
                        {
                            // txtInfo.Text = "";
                            updateBoard(d);
                            moveBall(d);
                            checkResult();
                        }
                    }
                }
                else
                {
                    if (Math.Abs(dx) > (Math.Abs(dy)))
                        if (dx > 0)
                            d = direction.Right;
                        else
                            d = direction.Left;
                    else
                      if (dy > 0)
                        d = direction.Down;
                    else
                        d = direction.Up;

                    if (isValidMove(d))
                    {
                        // txtInfo.Text = "";
                        updateBoard(d);
                        moveBall(d);
                        checkResult();
                    }
                    else
                    {
                        //txtInfo.Text = "Invalid move.";
                        if (popUp == null) popUp = new Popup();
                        MessageBox msgBox = new MessageBox("You cannot move in this position.", "Invalid move");
                        popUp.Child = msgBox;
                        if (!popUp.IsOpen)
                            popUp.IsOpen = true;
                    }
                }
            }
            ball_tapped = false;
        }
        private void updateBoard(direction d)
        {
            board[row, col] = 8;
            switch (d)
            {
                case direction.Left:
                    board[row, col - 1] = currPlayer - 1;
                    break;
                case direction.Up:
                    board[row - 1, col] = currPlayer - 1;
                    break;
                case direction.Right:
                    board[row, col + 1] = currPlayer - 1;
                    break;
                case direction.Down:
                    board[row + 1, col] = currPlayer - 1;
                    break;
                case direction.UpLeft:
                    board[row - 1, col-1] = currPlayer - 1;
                    break;
                case direction.UpRight:
                    board[row - 1, col+1] = currPlayer - 1;
                    break;
                case direction.DownLeft:
                    board[row + 1, col-1] = currPlayer - 1;
                    break;
                case direction.DownRight:
                    board[row +1, col+1] = currPlayer - 1;
                    break;
                default:
                    break;
            }
         }
        private void checkResult()
        {
            for(int i=0;i<5 && winner==0;i++)
            {
                if ((board[i, 0] + board[i, 1] + board[i, 2] + board[i, 3] + board[i, 4]) == 0)
                    winner = 1;
                if ((board[i, 0] + board[i, 1] + board[i, 2] + board[i, 3] + board[i, 4]) == 5)
                    winner = 2;

                if ((board[0, i] + board[1, i] + board[2, i] + board[3, i] + board[4, i]) == 0)
                    winner = 1;
                if ((board[0, i] + board[1, i] + board[2, i] + board[3, i] + board[4, i]) == 5)
                    winner = 2;
            }
            if ((board[0, 0] + board[1, 1] + board[2, 2] + board[3, 3] + board[4, 4]) == 0)
                winner = 1;
            if ((board[0, 0] + board[1, 1] + board[2, 2] + board[3, 3] + board[4, 4]) == 5)
                winner = 2;

            if ((board[0, 4] + board[1, 3] + board[2, 2] + board[3, 1] + board[4, 0]) == 0)
                winner = 1;
            if ((board[0, 4] + board[1, 3] + board[2, 2] + board[3, 1] + board[4, 0]) == 5)
                winner = 2;

            if (winner > 0)
            {
               // txtInfo.Text = "Player " + winner + " won.";
                gridMain.IsTapEnabled = false;
                txt1.Text = "Winner";
                if (winner == 1)
                {
                    turnOf1.Visibility = Visibility.Visible;
                    turnOf2.Visibility = Visibility.Collapsed;
                }else
                {
                    turnOf2.Visibility = Visibility.Visible;
                    turnOf1.Visibility = Visibility.Collapsed;
                }
                
                if (popUp == null) popUp = new Popup();
                MessageBox msgBox = new MessageBox("Winner: Player"+winner+"","Game over");
                popUp.Child = msgBox;
                if (!popUp.IsOpen)
                    popUp.IsOpen = true;
                btnPlay.Visibility = Visibility.Visible;
            }
        }
        enum direction { Left,Up,Right,Down,UpLeft,UpRight,DownLeft,DownRight};

        private void Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            checkResult();
        }

        private void btnPlay_Tapped(object sender, TappedRoutedEventArgs e)
        {
            sbBall.Stop();
            Frame.Navigate(typeof(BuddhiChal),pMode);
        }
        
        private void back_Tapped(object sender, TappedRoutedEventArgs e)
        {
            sbBall.Stop();
            Frame.Navigate(typeof(MainPage));
        }

        private void moveBall(direction d)
        {
            // try { sbBall.Stop(); } catch { }

            
               // txtInfo.Text = "";
                Point newPoint = p1;
            switch (d)
            {
                case direction.Left:
                    newPoint.X -= scale;
                    break;
                case direction.Up:
                    newPoint.Y -= scale;
                    break;
                case direction.Right:
                    newPoint.X += scale;
                    break;
                case direction.Down:
                    newPoint.Y += scale;
                    break;
                case direction.UpLeft:
                    newPoint.X -= scale;
                    newPoint.Y -= scale;
                    break;
                case direction.UpRight:
                    newPoint.Y -= scale;
                    newPoint.X += scale;
                    break;
                case direction.DownLeft:
                    newPoint.Y += scale;
                    newPoint.X -= scale;
                    break;
                case direction.DownRight:
                    newPoint.Y += scale;
                    newPoint.X += scale;
                    break;
                default:
                    break;
            }

            
                //current.CornerRadius = new CornerRadius(50);
                //current.RadiusX = 20;
                //current.RadiusY = 20;
                currAnim.To = newPoint;
                current.StrokeThickness = 0;
                p1.X = 0;
                sbBall.Begin();
                currPlayer = currPlayer == 1 ? 2 : 1;
                changeTurn(currPlayer == 1);
            
        }
        private void Ball_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if(playMode==2)
                if ((redOnHand > 0 && currPlayer==2) || (blueOnHand > 0 && currPlayer == 1))
                    return;

            ball_tapped = true;
            current = sender as Windows.UI.Xaml.Shapes.Path;
            if (currPlayer == 1 && current.Name.Contains("red"))
                return;
            if (currPlayer == 2 && current.Name.Contains("blue"))
                return;

            //Border brdr = sender as Border;
            p1 = e.GetPosition(gridMain);
            col = (int)(p1.X / scale);
            row = (int)(p1.Y / scale);
            p1 = new Point((scale/2) + (col * scale), (scale/2) + (row * scale));

            switch (current.Name)
            {
                case "redBall1":
                    currAnim = pAnimR1;
                    break;
                case "redBall2":
                    currAnim = pAnimR2;
                    break;
                case "redBall3":
                    currAnim = pAnimR3;
                    break;
                case "redBall4":
                    currAnim = pAnimR4;
                    break;
                case "redBall5":
                    currAnim = pAnimR5;
                    break;
                case "blueBall1":
                    currAnim = pAnimB1;
                    break;
                case "blueBall2":
                    currAnim = pAnimB2;
                    break;
                case "blueBall3":
                    currAnim = pAnimB3;
                    break;
                case "blueBall4":
                    currAnim = pAnimB4;
                    break;
                case "blueBall5":
                    currAnim = pAnimB5;
                    break;
            }
            //current.Stroke = new SolidColorBrush(Colors.Black);
            //if(current.Name!="")
            if(prev!=null)
                prev.StrokeThickness = scale/20;
           
            // prev.CornerRadius = new CornerRadius(50);
            // current.CornerRadius = new CornerRadius(10);

            current.StrokeThickness = scale/10;

            prev = current;
        }
    }
}
