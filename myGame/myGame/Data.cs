﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace meroGame
{
    public class Data
    {
        public static int scale;

        Data()
        {
            double ht = Window.Current.Bounds.Height;
            double wd = Window.Current.Bounds.Width;
            scale = (int)(Math.Min(ht, wd) / 5);
        }
        
    }
}
