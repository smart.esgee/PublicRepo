﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace meroGame
{
    public sealed partial class MessageBox : UserControl
    {
        public bool OkTapped { get; set; } = false;
        public MessageBox()
        {
            this.InitializeComponent();
            brdrMain.Width = Window.Current.Bounds.Width;
            brdrMain.Height = Window.Current.Bounds.Height;
        }
        public MessageBox(string text,string title="Message",bool ButtonOkCancel=false)
        {
            this.InitializeComponent();
            if (!ButtonOkCancel)
                col2.Width = new GridLength(0);
               // btnCancel.Visibility = Visibility.Visible;
            
            txtText.Text = text;
            txtTitle.Text = title;
            brdrMain.Width = Window.Current.Bounds.Width;
            brdrMain.Height = Window.Current.Bounds.Height;

        }


        private void close_popup()
        {
            Popup p = this.Parent as Popup;
            p.IsOpen = false;

        }

        private void OK_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if(col2.Width.Value>0)
                OkTapped = true;

            close_popup();
            
        }

        private void Cancel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            close_popup();
        }
        private void close_popup(object sender, TappedRoutedEventArgs e)
        {
            close_popup();
        }
        private void brdrMain_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }
    }
}
