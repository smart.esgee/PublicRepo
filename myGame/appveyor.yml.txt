version: 1.0.{build}
image: Visual Studio 2017
before_build:
- cmd: nuget restore myGame/meroGame.sln
build:
  project: myGame
  verbosity: minimal